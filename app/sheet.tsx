'use client';
import * as React from "react";
import {
  Spreadsheet,
  CellBase,
} from "react-spreadsheet";



const INITIAL_ROWS = 24;
const INITIAL_COLUMNS = 14;

type StringCell = CellBase<string | undefined>;

/** A two-dimensional array of given type T in rows and columns */
export type Matrix<T> = Array<Array<T | undefined>>;

/**
 * Creates an empty matrix with given rows and columns
 * @param rows - integer, the amount of rows the matrix should have
 * @param columns - integer, the amount of columns the matrix should have
 * @returns an empty matrix with given rows and columns
 */
export function createEmpty<T>(rows: number, columns: number): Matrix<T> {
  const matrix = Array(rows);
  for (let i = 0; i < rows; i++) {
    matrix[i] = Array(columns);
  }
  return matrix;
}

const EMPTY_DATA =  createEmpty<StringCell>(INITIAL_ROWS, INITIAL_COLUMNS);


export default function Home() {
  const [data, setData] = React.useState(EMPTY_DATA);

  return (
    <Spreadsheet data={data} onChange={setData} />
  )
}
